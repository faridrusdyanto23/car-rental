-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2020 at 06:42 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project2`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `nik` bigint(16) NOT NULL,
  `nama_akun` varchar(32) NOT NULL,
  `alamat` varchar(120) NOT NULL,
  `telp` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`nik`, `nama_akun`, `alamat`, `telp`) VALUES
(1118100002, 'Far', 'Perum', '085642474998'),
(1118100003, 'Dira', 'Cibodas', '081288748505'),
(1118100004, 'Iwan', 'Kota Bumi', '089111808333'),
(1118100005, 'Fitri', 'Pasar Kemis', '087888101987'),
(1118100006, 'Hiyori', 'Wano', '085777111333');

-- --------------------------------------------------------

--
-- Table structure for table `mobil`
--

CREATE TABLE `mobil` (
  `nmr_mobil` varchar(16) NOT NULL,
  `nama_mobil` varchar(16) NOT NULL,
  `status` varchar(16) NOT NULL,
  `tarif` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mobil`
--

INSERT INTO `mobil` (`nmr_mobil`, `nama_mobil`, `status`, `tarif`) VALUES
('A1212HFS', 'Avanza', 'Tersedia', 150000),
('A9999ACB', 'Jazzzzzz', 'Tersedia', 350000),
('AB5656FCS', 'Jazz', 'Tersedia', 200000),
('AD9999ZDC', 'Xpander', 'Tersedia', 300000),
('B3434CXN', 'Xenia', 'Tersedia', 100000),
('W8612KKV', 'Kijang Innova', 'Tersedia', 250000);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`) VALUES
(1, 'farid', 'rusdyanto', 'Farid Rusdyanto');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `nmr_transaksi` bigint(20) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `nmr_mobil` varchar(16) NOT NULL,
  `durasi` int(3) NOT NULL,
  `biaya` int(16) DEFAULT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `denda` int(16) DEFAULT NULL,
  `total` int(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`nmr_transaksi`, `nik`, `nmr_mobil`, `durasi`, `biaya`, `tgl_pinjam`, `tgl_kembali`, `denda`, `total`) VALUES
(1598191027493, 1118100006, 'W8612KKV', 2, 500000, '2020-08-23', '2020-08-25', 0, 500000),
(1598191182282, 1118100005, 'B3434CXN', 1, 100000, '2020-08-28', '2020-08-30', 200000, 300000),
(1598193663055, 1118100004, 'AD9999ZDC', 2, 600000, '2020-08-25', '2020-08-28', 600000, 1200000),
(1598193885298, 1118100003, 'B3434CXN', 1, 100000, '2020-08-20', '2020-08-23', 400000, 500000),
(1598419006646, 1118100002, 'AD9999ZDC', 2, 600000, '2020-08-26', '2020-08-29', 600000, 1200000),
(1598419237445, 1118100002, 'W8612KKV', 1, 250000, '2020-08-01', '2020-08-02', 0, 250000),
(1598451275892, 1118100006, 'W8612KKV', 2, 500000, '2020-08-25', '2020-08-28', 500000, 1000000),
(1598458493259, 1118100005, 'B3434CXN', 3, 300000, '2020-08-01', '2020-08-05', 200000, 500000),
(1598459031393, 1118100003, 'AB5656FCS', 1, 200000, '2020-08-04', '2020-08-06', 400000, 600000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `mobil`
--
ALTER TABLE `mobil`
  ADD PRIMARY KEY (`nmr_mobil`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`nmr_transaksi`),
  ADD KEY `nik` (`nik`),
  ADD KEY `nmr_mobil` (`nmr_mobil`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`nmr_mobil`) REFERENCES `mobil` (`nmr_mobil`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`nik`) REFERENCES `akun` (`nik`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
