/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.dao;

import com.farid.koneksi.Koneksi;
import com.farid.model.Akun;
import com.farid.model.Mobil;
import com.farid.model.Transaksi;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Farid
 */
public class TransaksiDao {
    
    Connection connection;
    
    public TransaksiDao(){
        connection = Koneksi.connection();
    }
    
    public boolean saveRent(Transaksi transaksi){
        boolean isSaved = true;
        PreparedStatement statement = null;
        String sql = "insert into transaksi "
                + "(nmr_transaksi, nik, nmr_mobil, durasi, biaya, tgl_pinjam) "
                + "values (?, ?, ?, ?, ?, ?)";
        try {
            statement = connection.prepareStatement(sql);
            statement.setLong(1, transaksi.getNmrTransaksi());
            statement.setLong(2, transaksi.getAkun().getNik());
            statement.setString(3, transaksi.getMobil().getNmrMobil());
            statement.setInt(4, transaksi.getDurasi());
            statement.setLong(5, transaksi.getBiaya());
            statement.setDate(6, new Date(transaksi.getTglPinjam().getTime()));
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            isSaved = false;
        }finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return isSaved;
    }
    
    public boolean saveReturn(Transaksi transaksi){
        boolean isSaved = true;
        PreparedStatement statement = null;
        String sql = "update transaksi set "
                + "denda = ?, tgl_kembali = ?, total = ? "
                + "where nmr_transaksi = ?";
        try {
            statement = connection.prepareStatement(sql);
            statement.setLong(1, transaksi.getDenda());
            statement.setDate(2, new Date(transaksi.getTglKembali().getTime()));
            statement.setLong(3, transaksi.getTotal());
            statement.setLong(4, transaksi.getNmrTransaksi());
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            isSaved = false;
        }finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return isSaved;
    }
    
    public boolean updateStatusMobil(Transaksi transaksi, String statusMobil){
        boolean isSaved = true;
        PreparedStatement statement = null;
        String sql = "update mobil "
                + "status = ? "
                + "where nmr_mobil = ?";
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, statusMobil);
            statement.setString(2, transaksi.getMobil().getNmrMobil());
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            isSaved = false;
        }finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return isSaved;
    }
    
    public List<Transaksi> getTable() {
        List<Transaksi> lb = new ArrayList<Transaksi>();
        String sql = "SELECT * FROM transaksi t "
                + "left join akun a on t.nik = a.nik "
                + "left join mobil m on t.nmr_mobil = m.nmr_mobil";
        try{
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Transaksi b = new Transaksi();
                Akun akun = new Akun();
                Mobil mobil = new Mobil();
                
                akun.setAlamat(rs.getString("alamat"));
                akun.setNamaAkun(rs.getString("nama_akun"));
                akun.setNik(rs.getLong("nik"));
                akun.setTelp(rs.getString("telp"));
                
                mobil.setNmrMobil(rs.getString("nmr_mobil"));
                mobil.setNamaMobil(rs.getString("nama_mobil"));
                mobil.setStatus(rs.getString("status"));
                mobil.setTarif(rs.getInt("tarif"));
                
                
                b.setNmrTransaksi(rs.getLong("nmr_transaksi"));
                b.setAkun(akun);
                b.setMobil(mobil);
                b.setDenda(rs.getLong("denda"));
                b.setBiaya(rs.getLong("biaya"));
                b.setDurasi(rs.getInt("durasi"));
                b.setTglPinjam(rs.getDate("tgl_pinjam"));
                b.setTglKembali(rs.getDate("tgl_kembali"));
                b.setTotal(rs.getLong("total"));
                lb.add(b);
            }
            
        } catch(SQLException ex){
            System.err.println(ex);
        } 
        return lb;
    }
    
    public Transaksi getTrxByNoTrx(String noTrx) {
        Transaksi trx = null;
        String sql = "SELECT * FROM transaksi t "
                + "left join akun a on t.nik = a.nik "
                + "left join mobil m on t.nmr_mobil = m.nmr_mobil "
                + "where nmr_transaksi = ?";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, noTrx);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                trx = new Transaksi();
                Akun akun = new Akun();
                Mobil mobil = new Mobil();
                
                akun.setAlamat(rs.getString("alamat"));
                akun.setNamaAkun(rs.getString("nama_akun"));
                akun.setNik(rs.getLong("nik"));
                akun.setTelp(rs.getString("telp"));
                
                mobil.setNmrMobil(rs.getString("nmr_mobil"));
                mobil.setNamaMobil(rs.getString("nama_mobil"));
                mobil.setStatus(rs.getString("status"));
                mobil.setTarif(rs.getInt("tarif"));
                
                
                trx.setNmrTransaksi(rs.getLong("nmr_transaksi"));
                trx.setAkun(akun);
                trx.setMobil(mobil);
                trx.setDenda(rs.getLong("denda"));
                trx.setBiaya(rs.getLong("biaya"));
                trx.setDurasi(rs.getInt("durasi"));
                trx.setTglPinjam(rs.getDate("tgl_pinjam"));
                trx.setTglKembali(rs.getDate("tgl_kembali"));
                trx.setTotal(rs.getLong("total"));
                break;
            }
            
        } catch(SQLException ex){
            System.err.println(ex);
        } 
        return trx;
    }
    
    public List<Transaksi> getTableByDate(java.util.Date start, java.util.Date end) {
        List<Transaksi> lb = new ArrayList<Transaksi>();
        String sql = "SELECT * FROM transaksi t "
                + "left join akun a on t.nik = a.nik "
                + "left join mobil m on t.nmr_mobil = m.nmr_mobil "
                + "where t.tgl_pinjam between ? and ? ";
        try{
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setDate(1, new Date(start.getTime()));
            statement.setDate(2, new Date(end.getTime()));
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Transaksi b = new Transaksi();
                Akun akun = new Akun();
                Mobil mobil = new Mobil();
                
                akun.setAlamat(rs.getString("alamat"));
                akun.setNamaAkun(rs.getString("nama_akun"));
                akun.setNik(rs.getLong("nik"));
                akun.setTelp(rs.getString("telp"));
                
                mobil.setNmrMobil(rs.getString("nmr_mobil"));
                mobil.setNamaMobil(rs.getString("nama_mobil"));
                mobil.setStatus(rs.getString("status"));
                mobil.setTarif(rs.getInt("tarif"));
                
                
                b.setNmrTransaksi(rs.getLong("nmr_transaksi"));
                b.setAkun(akun);
                b.setMobil(mobil);
                b.setDenda(rs.getLong("denda"));
                b.setBiaya(rs.getLong("biaya"));
                b.setDurasi(rs.getInt("durasi"));
                b.setTglPinjam(rs.getDate("tgl_pinjam"));
                b.setTglKembali(rs.getDate("tgl_kembali"));
                b.setTotal(rs.getLong("total"));
                lb.add(b);
            }
            
        } catch(SQLException ex){
            System.err.println(ex);
        } 
        return lb;
    }
}

