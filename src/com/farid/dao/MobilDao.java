/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.dao;

import com.farid.model.Mobil;
import java.util.List;

/**
 *
 * @author Farid
 */
public interface MobilDao {
    public void insert(Mobil b);
    public void update(Mobil b);
    public void delete(String nmrMobil);
    public List<Mobil> getAll();
    public List<Mobil> getCariNmrMobil(String nmr_mobil);
    public Mobil getCariNmrMbl(String nmr_mobil);
    public List<Mobil> getAllStatus();
}
