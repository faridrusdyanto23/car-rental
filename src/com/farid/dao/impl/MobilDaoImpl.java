/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.dao.impl;

import com.farid.koneksi.Koneksi;
import com.farid.model.Mobil;
import com.farid.dao.MobilDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Farid
 */
public class MobilDaoImpl implements MobilDao{
    
    Connection connection;
   
    public MobilDaoImpl(){
        connection = Koneksi.connection();
    }

    @Override
    public void insert(Mobil b) {
        PreparedStatement statement = null;
        String sql = "INSERT INTO mobil (nmr_mobil, nama_mobil, status, tarif) VALUES (?, ?, ?, ?)";
        try{
            statement = Koneksi.connection().prepareStatement(sql);
            statement.setString(1, b.getNmrMobil());
            statement.setString(2, b.getNamaMobil());
            statement.setString(3, b.getStatus());
            statement.setInt(4, b.getTarif());
            statement.executeUpdate();
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } finally {
            try{
                statement.close();
            } catch(SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void update(Mobil b) {
        PreparedStatement statement = null;
        String sql = "UPDATE mobil set nama_mobil=?, status=?, tarif=? WHERE nmr_mobil=?";
        try{
            statement = connection.prepareStatement(sql);
            
            statement.setString(1, b.getNamaMobil());
            statement.setString(2, b.getStatus());
            statement.setInt(3, b.getTarif());
            statement.setString(4, b.getNmrMobil());
            statement.executeUpdate();
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } finally {
            try{
                statement.close();
            } catch(SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void delete(String nmrMobil) {
        PreparedStatement statement = null;
        String sql = "DELETE FROM mobil WHERE nmr_mobil=?";
        try{
            statement = connection.prepareStatement(sql);
            statement.setString(1, nmrMobil);
            statement.executeUpdate();
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } finally {
            try{
                statement.close();
            } catch(SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public List<Mobil> getAll() {
        List<Mobil> lb = null;
        String sql = "SELECT * FROM mobil where status='tersedia'";
        try{
            lb = new ArrayList<Mobil>();
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Mobil b = new Mobil();
                b.setNmrMobil(rs.getString("nmr_mobil"));
                b.setNamaMobil(rs.getString("nama_mobil"));
                b.setStatus(rs.getString("status"));
                b.setTarif(rs.getInt("tarif"));
                lb.add(b);
            }
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } 
        return lb;
    }
    
    @Override
    public List<Mobil> getAllStatus() {
        List<Mobil> lb = null;
        String sql = "SELECT * FROM mobil";
        try{
            lb = new ArrayList<Mobil>();
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Mobil b = new Mobil();
                b.setNmrMobil(rs.getString("nmr_mobil"));
                b.setNamaMobil(rs.getString("nama_mobil"));
                b.setStatus(rs.getString("status"));
                b.setTarif(rs.getInt("tarif"));
                lb.add(b);
            }
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } 
        return lb;
    }

    @Override
    public List<Mobil> getCariNmrMobil(String nmr_mobil) {
        List<Mobil> lb = null;
        String sql = "SELECT * FROM mobil WHERE nmr_mobil LIKE ?";
        try{
            lb = new ArrayList<Mobil>();
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + nmr_mobil + "%");
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Mobil b = new Mobil();
                b.setNmrMobil(rs.getString("nmr_mobil"));
                b.setNamaMobil(rs.getString("nama_mobil"));
                b.setStatus(rs.getString("status"));
                b.setTarif(rs.getInt("tarif"));
                lb.add(b);
            }
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } 
        return lb;
    }
    
    @Override
    public Mobil getCariNmrMbl(String nmr_mobil) {
        Mobil b = null;
        String sql = "SELECT * FROM mobil WHERE nmr_mobil LIKE ?";
        try{
            
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + nmr_mobil + "%");
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                b = new Mobil();
                b.setNmrMobil(rs.getString("nmr_mobil"));
                b.setNamaMobil(rs.getString("nama_mobil"));
                b.setStatus(rs.getString("status"));
                b.setTarif(rs.getInt("tarif"));
                break;
            }
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } 
        return b;
    }
}
