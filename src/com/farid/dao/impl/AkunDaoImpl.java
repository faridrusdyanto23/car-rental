/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.dao.impl;

import com.farid.koneksi.Koneksi;
import com.farid.model.Akun;
import com.farid.dao.AkunDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Farid
 */
public class AkunDaoImpl implements AkunDao{
    
    Connection connection;
    
    public AkunDaoImpl(){
        connection = Koneksi.connection();
    }
    
    @Override
    public void insert(Akun b) {
        PreparedStatement statement = null;
        String sql = "INSERT INTO akun (nik, nama_akun, alamat, telp) "
                    + "VALUES (?, ?, ?, ?)";
        try{
            statement = connection.prepareStatement(sql);
            statement.setLong(1, b.getNik());
            statement.setString(2, b.getNamaAkun());
            statement.setString(3, b.getAlamat());
            statement.setString(4, b.getTelp());
            statement.executeUpdate();
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } finally {
            try{
                statement.close();
            } catch(SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void update(Akun b) {
        PreparedStatement statement = null;
        String sql = "UPDATE akun set nama_akun=?, alamat=?, telp=? WHERE nik=?";
        try{
            statement = connection.prepareStatement(sql);
            
            statement.setString(1, b.getNamaAkun());
            statement.setString(2, b.getAlamat());
            statement.setString(3, b.getTelp());
            statement.setLong(4, b.getNik());
            statement.executeUpdate();
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } finally {
            try{
                statement.close();
            } catch(SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void delete(int nik) {
        PreparedStatement statement = null;
        String sql = "DELETE FROM akun WHERE nik=?";
        try{
            statement = connection.prepareStatement(sql);
            statement.setInt(1, nik);
            statement.executeUpdate();
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } finally {
            try{
                statement.close();
            } catch(SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public List<Akun> getAll() {
        List<Akun> lb = null;
        String sql = "SELECT * FROM akun";
        try{
            lb = new ArrayList<Akun>();
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                Akun b = new Akun();
                b.setNik(rs.getLong("nik"));
                b.setNamaAkun(rs.getString("nama_akun"));
                b.setAlamat(rs.getString("alamat"));
                b.setTelp(rs.getString("telp"));
                lb.add(b);
            }
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } 
        return lb;
    }

    @Override
    public List<Akun> getCariNik(String nik) {
        List<Akun> lb = null;
        String sql = "SELECT * FROM akun WHERE nik LIKE ?";
        try{
            lb = new ArrayList<Akun>();
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + nik + "%");
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Akun b = new Akun();
                b.setNik(rs.getLong("nik"));
                b.setNamaAkun(rs.getString("nama_akun"));
                b.setAlamat(rs.getString("alamat"));
                b.setTelp(rs.getString("telp"));
                lb.add(b);
            }
            
        } catch(SQLException ex){
            ex.printStackTrace();
        } 
        return lb;
    }

    @Override
    public Akun getAkunByNik(String nik) {
        Akun akun = null;
        String sql = "SELECT * FROM akun WHERE nik = ?";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, nik);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                akun = new Akun();
                akun.setNik(rs.getLong("nik"));
                akun.setNamaAkun(rs.getString("nama_akun"));
                akun.setAlamat(rs.getString("alamat"));
                akun.setTelp(rs.getString("telp"));
                break;
            }
            
        } catch(SQLException ex){
            System.err.println(ex);
        } 
        return akun;
    }
}
