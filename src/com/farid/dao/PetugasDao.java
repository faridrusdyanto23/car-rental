/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.dao;

import com.farid.koneksi.Koneksi;
import com.farid.model.Petugas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Farid
 */
public class PetugasDao {
    Connection connection;
    
    public PetugasDao(){
        connection = Koneksi.connection();
    }
    
    public Petugas login(String userName, String password){
        Petugas petugas = null;
        PreparedStatement statement = null;
        String sql = "select * from petugas where username = ? and password = ?";
        
        try{
            statement = connection.prepareStatement(sql);
            statement.setString(1, userName);
            statement.setString(2, password);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                petugas = new Petugas();
                petugas.setIdPetugas(rs.getLong("id_petugas"));
                petugas.setNamaPetugas(rs.getString("nama_petugas"));
                petugas.setPassword(rs.getString("password"));
                petugas.setUserName(rs.getString("username"));
                break;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            
        }finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return petugas;
    }
    
}
