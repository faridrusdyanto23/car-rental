/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.dao;

import com.farid.model.Akun;
import java.util.List;

/**
 *
 * @author Farid
 */
public interface AkunDao {
    public void insert(Akun b);
    public void update(Akun b);
    public void delete(int id);
    public List<Akun> getAll();
    public List<Akun> getCariNik(String nik);
    public Akun getAkunByNik(String nik);
}
