/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.service;

import com.farid.dao.TransaksiDao;
import com.farid.dao.impl.AkunDaoImpl;
import com.farid.dao.impl.MobilDaoImpl;
import com.farid.model.Akun;
import com.farid.model.Mobil;
import com.farid.model.Transaksi;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author Farid
 */

public class TransaksiService {
    
    // Menghitung Biaya Transaksi
    public Long calculateAmount(Transaksi transaksi){
        Long amount = transaksi.getDurasi().longValue() * transaksi.getMobil().getTarif();
        return amount;
    }
    
    // Menghitung Denda
    public Long calculatePinalty(Transaksi transaksi){
        Date returnDate = transaksi.getTglKembali();
        Date rentDate = transaksi.getTglPinjam();
        Long pinaltyNominal = 0l;
        try {
            Long diffTime = returnDate.getTime() - rentDate.getTime();
            Long days = (diffTime / (1000 * 60 * 60 * 24)) + 1;
            Long pinalty = days - (transaksi.getDurasi() + 1);
            
            if (pinalty != 0) {
                pinaltyNominal = pinalty * (2l * transaksi.getMobil().getTarif());
            } 
        } catch (Exception e) {
            System.err.println("Error hitung denda");
        }
        return pinaltyNominal;
    }
    
    // Menghitung total
    public Long calculateTotal(Transaksi transaksi){
        Long total = transaksi.getBiaya() + transaksi.getDenda();
        
        return total;
    }
    
    // update status mobil
    public Mobil updateStatusMobil(boolean isRent, Mobil mobil){
        if (isRent){
            mobil.setStatus("Tidak Tersedia");
        } else {
            mobil.setStatus("Tersedia");
        }
        MobilDaoImpl mobilDaoimpl = new MobilDaoImpl();
        mobilDaoimpl.update(mobil);
        return mobil;
    }
    
    // Transaksi Pinjam
    public Boolean doRent(Map<String,Object> mapInput){
        MobilDaoImpl mdi = new MobilDaoImpl();
        Mobil mobil = mdi.getCariNmrMbl((String)mapInput.get("nmrMobil"));
        mobil = updateStatusMobil(true, mobil);
        
        AkunDaoImpl ad = new AkunDaoImpl();
        Akun akun = ad.getAkunByNik((String) mapInput.get("nik"));
        
        Transaksi trx = new Transaksi();
        trx.setAkun(akun);
        trx.setMobil(mobil);
        trx.setDurasi((Integer) mapInput.get("durasi"));
        trx.setNmrTransaksi(System.currentTimeMillis());
        trx.setTglPinjam((Date) mapInput.get("tglPinjam"));
        
        Long biaya = calculateAmount(trx);
        trx.setBiaya(biaya);
        
        TransaksiDao trxDao = new TransaksiDao();
        return trxDao.saveRent(trx);
        
    }
    
    // Transaksi Kembali (update)
    public Boolean doReturn(Map<String,Object> mapInput){
        TransaksiDao td = new TransaksiDao();
        Transaksi trx = td.getTrxByNoTrx((String) mapInput.get("noTrx"));
        
        Mobil mobil = trx.getMobil();
        mobil = updateStatusMobil(false, mobil);
        
        trx.setTglKembali((Date) mapInput.get("tglKembali"));
        Long denda = calculatePinalty(trx);
        trx.setDenda(denda);
        
        Long total = calculateTotal(trx);
        trx.setTotal(total);
        
        TransaksiDao trxDao = new TransaksiDao();
        return trxDao.saveReturn(trx);
    }
}
