/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author Farid
 */
public class AkunTableModel extends AbstractTableModel{
    
    List<Akun> lb;
    
    public AkunTableModel(List<Akun> lb){
        this.lb = lb;
    }

    @Override
    public int getRowCount() {
        return lb.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }
    
    @Override
    public String getColumnName(int column){
        switch(column){
            case 0:
                return "NIK";
            case 1:
                return "Nama Akun";
            case 2:
                return "Alamat";
            case 3:
                return "Telp";
            default:
                return null;
            
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column){
            case 0:
                return lb.get(row).getNik();
            case 1:
                return lb.get(row).getNamaAkun();
            case 2:
                return lb.get(row).getAlamat();
            case 3:
                return lb.get(row).getTelp();
            case 4:
                return lb.get(row).getAlamat();
            default:
                return null;
        }
    }
    
}
