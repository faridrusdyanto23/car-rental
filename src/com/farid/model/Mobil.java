/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.model;

/**
 *
 * @author Farid
 */
public class Mobil {
    private String nmrMobil;
    private String namaMobil;
    private String status;
    private Integer tarif;

    public String getNmrMobil() {
        return nmrMobil;
    }

    public void setNmrMobil(String nmrMobil) {
        this.nmrMobil = nmrMobil;
    }

    public String getNamaMobil() {
        return namaMobil;
    }

    public void setNamaMobil(String namaMobil) {
        this.namaMobil = namaMobil;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTarif() {
        return tarif;
    }

    public void setTarif(Integer tarif) {
        this.tarif = tarif;
    }
    
}
