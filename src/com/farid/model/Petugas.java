/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.model;

/**
 *
 * @author Farid
 */
public class Petugas {
    private Long idPetugas;
    private String userName;
    private String password;
    private String namaPetugas;

    public Long getIdPetugas() {
        return idPetugas;
    }

    public void setIdPetugas(Long idPetugas) {
        this.idPetugas = idPetugas;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNamaPetugas() {
        return namaPetugas;
    }

    public void setNamaPetugas(String namaPetugas) {
        this.namaPetugas = namaPetugas;
    }
    
    
}
