/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author Farid
 */
public class MobilTableModel extends AbstractTableModel{
    
    List<Mobil> lb;
    
    public MobilTableModel(List<Mobil> lb){
        this.lb = lb;
    }
    
    @Override
    public int getRowCount() {
        return lb.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }
    
    @Override
    public String getColumnName(int column){
        switch(column){
            case 0:
                return "No.Mobil";
            case 1:
                return "Nama Mobil";
            case 2:
                return "Status";
            case 3:
                return "Tarif";
            default:
                return null;
            
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column){
            case 0:
                return lb.get(row).getNmrMobil();
            case 1:
                return lb.get(row).getNamaMobil();
            case 2:
                return lb.get(row).getStatus();
            case 3:
                return lb.get(row).getTarif();
            default:
                return null;
        }
    }
    
}
