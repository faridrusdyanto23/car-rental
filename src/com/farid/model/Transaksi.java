/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.model;

import java.util.Date;

/**
 *
 * @author Farid
 */
public class Transaksi {
    private Long nmrTransaksi;
    private Akun akun;
    private Mobil mobil;
    private Integer durasi;
    private Long biaya;
    private Date tglPinjam;
    private Date tglKembali;
    private Long denda;
    private Long total;

    public Long getNmrTransaksi() {
        return nmrTransaksi;
    }

    public void setNmrTransaksi(Long nmrTransaksi) {
        this.nmrTransaksi = nmrTransaksi;
    }

    public Akun getAkun() {
        return akun;
    }

    public void setAkun(Akun akun) {
        this.akun = akun;
    }

    public Mobil getMobil() {
        return mobil;
    }

    public void setMobil(Mobil mobil) {
        this.mobil = mobil;
    }

    public Integer getDurasi() {
        return durasi;
    }

    public void setDurasi(Integer durasi) {
        this.durasi = durasi;
    }

    public Long getBiaya() {
        return biaya;
    }

    public void setBiaya(Long biaya) {
        this.biaya = biaya;
    }

    public Date getTglPinjam() {
        return tglPinjam;
    }

    public void setTglPinjam(Date tglPinjam) {
        this.tglPinjam = tglPinjam;
    }

    public Date getTglKembali() {
        return tglKembali;
    }

    public void setTglKembali(Date tglKembali) {
        this.tglKembali = tglKembali;
    }

    public Long getDenda() {
        return denda;
    }

    public void setDenda(Long denda) {
        this.denda = denda;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
    
}
