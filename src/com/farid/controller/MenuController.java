/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.controller;

import com.farid.view.FrameAkun;
import com.farid.view.FrameLaporan;
import com.farid.view.FrameMenu;
import com.farid.view.FrameMobil;
import com.farid.view.FrameLogin;
import com.farid.view.FrameTransaksi;
import javax.swing.JFrame;
/**
 *
 * @author Farid
 */
public class MenuController {
    FrameMenu frame;

    public MenuController(FrameMenu frame) {
        this.frame = frame;
        
    }
        public void getFrameAkun(){
            JFrame f = new FrameAkun();
            f.setLocationRelativeTo(null);
            f.setVisible(true);
            frame.setVisible(false);
        }
        
        public void getFrameMobil(){
            JFrame f = new FrameMobil();
            f.setLocationRelativeTo(null);
            f.setVisible(true);
            frame.setVisible(false);
        }
        
        public void getFrameTransaksi(){
            JFrame f = new FrameTransaksi();
            f.setLocationRelativeTo(null);
            f.setVisible(true);
            frame.setVisible(false);
        }
        
        public void getFrameLogin(){
            JFrame f = new FrameLogin();
            f.setLocationRelativeTo(null);
            f.setVisible(true);
            frame.setVisible(false);
        }
        
        public void getFrameLaporan(){
            JFrame f = new FrameLaporan();
            f.setLocationRelativeTo(null);
            f.setVisible(true);
            frame.setVisible(false);
        }
}
