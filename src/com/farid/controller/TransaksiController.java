/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.controller;

import com.farid.dao.TransaksiDao;
import com.farid.dao.impl.AkunDaoImpl;
import com.farid.dao.impl.MobilDaoImpl;
import com.farid.model.Akun;
import com.farid.model.Mobil;
import com.farid.model.Transaksi;
import com.farid.service.TransaksiService;
import com.farid.view.FrameMenu;
import com.farid.view.FrameTransaksi;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Farid
 */
public class TransaksiController {
    FrameTransaksi frame;
    DefaultTableModel model;
    
    public TransaksiController(FrameTransaksi frame){
        this.frame = frame;
    }
    
    public void getDataTable(){
    model = (DefaultTableModel) frame.getTabelData().getModel();
        TransaksiDao td = new TransaksiDao();
        List<Transaksi> ls = td.getTable();
        
        for(int i = 0; i < ls.size(); i++ ){
            Transaksi trx = ls.get(i);
            Object[ ] obj = new Object[13];
            obj[0] = trx.getNmrTransaksi();
            obj[1] = trx.getAkun().getNik();
            obj[2] = trx.getAkun().getNamaAkun();
            obj[3] = trx.getAkun().getAlamat();
            obj[4] = trx.getMobil().getNmrMobil();
            obj[5] = trx.getMobil().getNamaMobil();
            obj[6] = trx.getMobil().getTarif();
            obj[7] = trx.getDurasi();
            obj[8] = trx.getBiaya();
            obj[9] = formattingDate(trx.getTglPinjam());
            obj[10] = formattingDate(trx.getTglKembali());
            obj[11] = trx.getDenda();
            obj[12] = trx.getTotal();
            model.addRow(obj);
        }
    }
    
    public String formattingDate(Date tmpDate) {
        String formatted = null;
        try {
            if (tmpDate != null) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                formatted = formatter.format(tmpDate);
            }
        } catch (Exception e) {
            System.out.println("Error Parse");
        }
        return formatted;
    }
    
    public void getDataTrxById(String nmrTrx) {
        model = (DefaultTableModel) frame.getTabelData().getModel();
        TransaksiDao td = new TransaksiDao();
        Transaksi trx = td.getTrxByNoTrx(nmrTrx);
            Object[ ] obj = new Object[13];
            obj[0] = trx.getNmrTransaksi();
            obj[1] = trx.getAkun().getNik();
            obj[2] = trx.getAkun().getNamaAkun();
            obj[3] = trx.getAkun().getAlamat();
            obj[4] = trx.getMobil().getNmrMobil();
            obj[5] = trx.getMobil().getNamaMobil();
            obj[6] = trx.getMobil().getTarif();
            obj[7] = trx.getDurasi();
            obj[8] = trx.getBiaya();
            obj[9] = formattingDate(trx.getTglPinjam());
            obj[10] = formattingDate(trx.getTglKembali());
            obj[11] = trx.getDenda();
            obj[12] = trx.getTotal();
            model.addRow(obj);
    }
    
    public void getCmbDataAkun(){
        frame.getTxtDataAkun().removeAllItems();
        AkunDaoImpl ak = new AkunDaoImpl();
        List<Akun> ls = ak.getAll();
        for(int i = 0; i < ls.size(); i++){
            frame.getTxtDataAkun().addItem(ls.get(i).getNik().toString());
        }
    }
    
    public void getCmbDataMobil(){
        frame.getTxtDataMobil().removeAllItems();
        MobilDaoImpl md = new MobilDaoImpl();
        List<Mobil> ls = md.getAll();
        for(int i = 0; i < ls.size(); i++){
            frame.getTxtDataMobil().addItem(ls.get(i).getNmrMobil());
        }
    }
    
    public void refresh(){
        model = (DefaultTableModel) frame.getTabelData().getModel();
        model.setRowCount(0);
        getDataTable();
        frame.getTxtCari().setText("");
        frame.getTxtTglKembali().setText("");
        getCmbDataMobil();
    }

    public void reset(){
        frame.getTxtNmrTrx().setText("");
        frame.getTxtDurasi().setText("");
        frame.getTxtTglPinjam().setText("");
        frame.getTxtTglKembali().setText("");
        frame.getTxtDataAkun().setEnabled(true);
        frame.getTxtDataMobil().setEnabled(true);
        frame.getTxtDurasi().setEditable(true);
        frame.getTxtTglPinjam().setEditable(true);
        frame.getButtonInsert().setEnabled(true);
        getCmbDataMobil();
    }
    
    public void cari() {
        if(!frame.getTxtCari().getText().trim().isEmpty()) {
            model = (DefaultTableModel) frame.getTabelData().getModel();
            model.setRowCount(0);
            getDataTrxById(frame.getTxtCari().getText());
        } else {
            JOptionPane.showMessageDialog(this.frame, 
                    "Masukan nomer Transaksi yang ingin dicari!","Notifikasi", JOptionPane.WARNING_MESSAGE);
        }
    }
    
    public void simpan() {
        if(!frame.getTxtDurasi().getText().trim().isEmpty() && !frame.getTxtTglPinjam().getText().trim().isEmpty()){
            
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date d;
            try {
                d = sdf.parse(frame.getTxtTglPinjam().getText());
            
                Map<String, Object> mapInput = new HashMap<>();
                mapInput.put("nik", frame.getTxtDataAkun().getSelectedItem());
                mapInput.put("nmrMobil", frame.getTxtDataMobil().getSelectedItem());
                mapInput.put("durasi", Integer.parseInt(frame.getTxtDurasi().getText()));
                mapInput.put("tglPinjam", d);

                TransaksiService ts = new TransaksiService();
                if(ts.doRent(mapInput)) {
                    refresh();
                    reset();
                    
                    JOptionPane.showMessageDialog(this.frame, 
                            "Transaksi berhasil ditambahkan","Notifikasi", JOptionPane.INFORMATION_MESSAGE);
                    
                } else {
                    JOptionPane.showMessageDialog(this.frame, 
                            "Transaksi gagal","Notifikasi", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }else {
            JOptionPane.showMessageDialog(this.frame, 
                    "Lengkapi form terlebih dahulu!","Notifikasi", JOptionPane.WARNING_MESSAGE);
            
        }
    }
    
    public void selesai() {
        
        if(!frame.getTxtNmrTrx().getText().trim().isEmpty() 
                && !frame.getTxtDurasi().getText().trim().isEmpty() 
                && !frame.getTxtTglPinjam().getText().trim().isEmpty() 
                && !frame.getTxtTglKembali().getText().trim().isEmpty()) {
            
            Map<String, Object> mapInput = new HashMap<>();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date d;
            
            try {
                d = sdf.parse(frame.getTxtTglKembali().getText());
                mapInput.put("tglKembali", d);
                mapInput.put("noTrx", frame.getTxtNmrTrx().getText());

                TransaksiService ts = new TransaksiService();
                if (ts.doReturn(mapInput)) {
                    refresh();
                    reset();
                    JOptionPane.showMessageDialog(this.frame, 
                            "Transaksi telah selesai","Notifikasi", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this.frame, 
                            "Transaksi gagal","Notifikasi", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(this.frame, 
                    "Lengkapi form terlebih dahulu!","Notifikasi", JOptionPane.WARNING_MESSAGE);
        }
    }
    
    public void menuUtama(){
        JFrame f = new FrameMenu();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
        frame.setVisible(false);
    }
    
    public void tableClick(){
        frame.getTxtNmrTrx().setText(frame.getTabelData().getValueAt(frame.getTabelData().getSelectedRow(), 0).toString());
        frame.getTxtDataAkun().setSelectedItem(frame.getTabelData().getValueAt(frame.getTabelData().getSelectedRow(), 1).toString());
        boolean isAdd = true;
        for (int i = 0; i < frame.getTxtDataMobil().getItemCount(); i++) {
            String nmrMobil = (String) frame.getTxtDataMobil().getItemAt(i);
            if (frame.getTabelData().getValueAt(frame.getTabelData().getSelectedRow(), 4).toString().equalsIgnoreCase(nmrMobil)) {
                isAdd = false;
                break;
            }
        }
        
        if (isAdd) {
            frame.getTxtDataMobil().addItem(frame.getTabelData().getValueAt(frame.getTabelData().getSelectedRow(), 4).toString());
        }
        frame.getTxtDataMobil().setSelectedItem(frame.getTabelData().getValueAt(frame.getTabelData().getSelectedRow(), 4).toString());
        frame.getTxtDurasi().setText(frame.getTabelData().getValueAt(frame.getTabelData().getSelectedRow(), 7).toString());
        frame.getTxtTglPinjam().setText(frame.getTabelData().getValueAt(frame.getTabelData().getSelectedRow(), 9).toString());
        frame.getTxtDataAkun().setEnabled(false);
        frame.getTxtDataMobil().setEnabled(false);
        frame.getTxtDurasi().setEditable(false);
        frame.getTxtTglPinjam().setEditable(false);
        frame.getButtonInsert().setEnabled(false);
    }
}
