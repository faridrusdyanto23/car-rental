/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.controller;

import com.farid.dao.MobilDao;
import com.farid.dao.impl.MobilDaoImpl;
import com.farid.model.Mobil;
import com.farid.model.MobilTableModel;
import com.farid.view.FrameMenu;
import com.farid.view.FrameMobil;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 *
 * @author Farid
 */
public class MobilController {
    FrameMobil frame;
    MobilDao mobilDao;
    List<Mobil> lb;
    
    public MobilController(FrameMobil frame){
        this.frame = frame;
        mobilDao = new MobilDaoImpl();
        lb = mobilDao.getAllStatus();
    }
    
    // Ke Menu Utama
    public void menuUtama(){
        JFrame f = new FrameMenu();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
        frame.setVisible(false);
    }
    
    // mengkosongkan field
    public void reset(){
        frame.getTxtNmrMobil().setText("");
        frame.getTxtNamaMobil().setText("");
        frame.getTxtStatus().setSelectedItem("");
        frame.getTxtTarif().setText("");
        frame.getTxtNmrMobil().setEditable(true);
        frame.getButtonInsert().setEnabled(true);
    }
    
    // menampilkan data kedalam tabel
    public void isiTable(){
        lb = mobilDao.getAllStatus();
        MobilTableModel tmb = new MobilTableModel(lb);
        frame.getTabelData().setModel(tmb);
    }
    
    public void refresh(){
        frame.getTxtCariNmrMobil().setText("");
        isiTable();
        frame.getTxtNmrMobil().setEditable(true);
        frame.getButtonInsert().setEnabled(true);
    }
    
    // merupakan fungsi untuk menampilkan data yang dipilih dari table
    public void isiField(){

        frame.getTxtNmrMobil().setText
        (frame.getTabelData().getModel().getValueAt(frame.getTabelData().getSelectedRow(), 0).toString());
        
        frame.getTxtNamaMobil().setText
        (frame.getTabelData().getModel().getValueAt(frame.getTabelData().getSelectedRow(), 1).toString());
        
        frame.getTxtStatus().setSelectedItem
        (frame.getTabelData().getModel().getValueAt(frame.getTabelData().getSelectedRow(), 2).toString());
        
        frame.getTxtTarif().setText
        (frame.getTabelData().getModel().getValueAt(frame.getTabelData().getSelectedRow(), 3).toString());
        
        frame.getTxtNmrMobil().setEditable(false);
        frame.getButtonInsert().setEnabled(false);
    }
    
    /* merupakan fungsi untuk insert data 
       berdasarkan inputan user dari textField di frame */
    public void insert(){
        
        if (!frame.getTxtNmrMobil().getText().trim().isEmpty() 
                & !frame.getTxtNamaMobil().getText().trim().isEmpty()
                & !frame.getTxtTarif().getText().trim().isEmpty()) {
            
            Mobil b = new Mobil();
            
            b.setNmrMobil(frame.getTxtNmrMobil().getText());
            b.setNamaMobil(frame.getTxtNamaMobil().getText());
            b.setStatus(frame.getTxtStatus().getSelectedItem().toString());
            b.setTarif(Integer.parseInt(frame.getTxtTarif().getText()));
            
            mobilDao.insert(b);
            JOptionPane.showMessageDialog(null, "Simpan Data Sukses");
            
        } else {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
    
    /* merupakan fungsi untuk update data 
       berdasarkan inputan user dari textField di frame */
    public void update(){
        
        if (!frame.getTxtNmrMobil().getText().trim().isEmpty() 
                & !frame.getTxtNamaMobil().getText().trim().isEmpty()
                & !frame.getTxtTarif().getText().trim().isEmpty()) {
            
            Mobil b = new Mobil();
            
            b.setNamaMobil(frame.getTxtNamaMobil().getText());
            b.setStatus(frame.getTxtStatus().getSelectedItem().toString());
            b.setTarif(Integer.parseInt(frame.getTxtTarif().getText()));
            b.setNmrMobil(frame.getTxtNmrMobil().getText());
            
            mobilDao.update(b);
            JOptionPane.showMessageDialog(null, "Update Data Sukses");
            
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih Data yang akan diubah");
        }
    }
    
    // Berfungsi menghapus data yang dipilih
    public void delete(){
        if (!frame.getTxtNmrMobil().getText().trim().isEmpty()) {
            
            mobilDao.delete(frame.getTxtNmrMobil().getText());
            
            JOptionPane.showMessageDialog(null, "Hapus Data Sukses");
            
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih Data yang akan dihapus");
        }
    }
    
    public void isiTableCariNmrMobil() {
        lb = mobilDao.getCariNmrMobil(frame.getTxtCariNmrMobil().getText());
        MobilTableModel tmb = new MobilTableModel(lb);
        frame.getTabelData().setModel(tmb);
    }
    
    public void cariNmrMobil() {
        if (!frame.getTxtCariNmrMobil().getText().trim().isEmpty()) {
            mobilDao.getCariNmrMobil(frame.getTxtCariNmrMobil().getText());
            isiTableCariNmrMobil();
            
        } else {
            JOptionPane.showMessageDialog(frame, "Kolom pencarian tidak boleh kosong");
        }
    }
}
