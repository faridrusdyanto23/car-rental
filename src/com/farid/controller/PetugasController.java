/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.controller;

import com.farid.dao.PetugasDao;
import com.farid.model.Petugas;
import com.farid.view.FrameLogin;
import com.farid.view.FrameMenu;
import javax.swing.JOptionPane;


/**
 *
 * @author Farid
 */
public class PetugasController {
    
    FrameLogin frame;
    
    public PetugasController(FrameLogin frame){
        this.frame = frame;
    }
    
    public void validasi(){
        PetugasDao pt = new PetugasDao();
        Petugas petugas = pt.login(frame.getTxtUserName().getText(), frame.getTxtPassword().getText());
        if (petugas != null){
            JOptionPane.showMessageDialog(this.frame,"Hallo " + petugas.getNamaPetugas());
            this.frame.dispose();
            FrameMenu x = new FrameMenu();
            x.setLocationRelativeTo(null);
            this.frame.setVisible(false);
            x.setVisible(true);     //  rootPaneCheckingEnabled

        } else{
            JOptionPane.showMessageDialog(this.frame,"Username atau Password salah");
        }
    }
}
