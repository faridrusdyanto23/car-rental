/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.farid.controller;

import com.farid.dao.AkunDao;
import com.farid.dao.impl.AkunDaoImpl;
import com.farid.model.Akun;
import com.farid.model.AkunTableModel;
import com.farid.view.FrameAkun;
import com.farid.view.FrameMenu;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 *
 * @author Farid
 */
public class AkunController {
    
    FrameAkun frame;
    AkunDao akunDao;
    List<Akun> lb;
    
    public AkunController(FrameAkun frame){
        this.frame = frame;
        akunDao = new AkunDaoImpl();
        lb = akunDao.getAll();
    }
    
    // Ke Menu Utama
    public void menuUtama(){
        JFrame f = new FrameMenu();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
        frame.setVisible(false);
    }
    
    // mengkosongkan field
    public void reset(){
        frame.getTxtNIK().setText("");
        frame.getTxtNama().setText("");
        frame.getTxtAlamat().setText("");
        frame.getTxtTelp().setText("");
        frame.getTxtNIK().setEditable(true);
        frame.getButtonInsert().setEnabled(true);
    }
    
    // menampilkan data kedalam tabel
    public void isiTable(){
        lb = akunDao.getAll();
        AkunTableModel tmb = new AkunTableModel(lb);
        frame.getTabelData().setModel(tmb);
    }
    
    public void refresh(){
        frame.getTxtCariNik().setText("");
        isiTable();
        frame.getTxtNIK().setEditable(true);
        frame.getButtonInsert().setEnabled(true);
    }
    
    // merupakan fungsi untuk menampilkan data yang dipilih dari table
    public void isiField(){

        frame.getTxtNIK().setText
        (frame.getTabelData().getModel().getValueAt(frame.getTabelData().getSelectedRow(), 0).toString());
        
        frame.getTxtNama().setText
        (frame.getTabelData().getModel().getValueAt(frame.getTabelData().getSelectedRow(), 1).toString());
        
        frame.getTxtAlamat().setText
        (frame.getTabelData().getModel().getValueAt(frame.getTabelData().getSelectedRow(), 2).toString());
        
        frame.getTxtTelp().setText
        (frame.getTabelData().getModel().getValueAt(frame.getTabelData().getSelectedRow(), 3).toString());
        
        frame.getTxtNIK().setEditable(false);
        frame.getButtonInsert().setEnabled(false);
    }
    
    /* merupakan fungsi untuk insert data 
       berdasarkan inputan user dari textField di frame */
    public void insert(){
        
        if (!frame.getTxtNIK().getText().trim().isEmpty() 
                & !frame.getTxtNama().getText().trim().isEmpty()
                & !frame.getTxtAlamat().getText().trim().isEmpty()
                & !frame.getTxtTelp().getText().trim().isEmpty()) {
            
            Akun b = new Akun();
            
            b.setNik(Long.parseLong(frame.getTxtNIK().getText()));
            b.setNamaAkun(frame.getTxtNama().getText());
            b.setAlamat(frame.getTxtAlamat().getText());
            b.setTelp(frame.getTxtTelp().getText());
            
            akunDao.insert(b);
            JOptionPane.showMessageDialog(null, "Simpan Data Sukses");
            
        } else {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }
    
    /* merupakan fungsi untuk update data 
       berdasarkan inputan user dari textField di frame */
    public void update(){
        
        if (!frame.getTxtNIK().getText().trim().isEmpty() 
                & !frame.getTxtNama().getText().trim().isEmpty()
                & !frame.getTxtAlamat().getText().trim().isEmpty()
                & !frame.getTxtTelp().getText().trim().isEmpty()) {
            
            Akun b = new Akun();
            
            b.setNamaAkun(frame.getTxtNama().getText());
            b.setAlamat(frame.getTxtAlamat().getText());
            b.setTelp(frame.getTxtTelp().getText());
            b.setNik(Long.parseLong(frame.getTxtNIK().getText()));
            
            akunDao.update(b);
            JOptionPane.showMessageDialog(null, "Update Data Sukses");
            
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih Data yang akan diubah");
        }
    }
    
    // Berfungsi menghapus data yang dipilih
    public void delete(){
        if (!frame.getTxtNIK().getText().trim().isEmpty()) {
            
            int id = Integer.parseInt(frame.getTxtNIK().getText());
            akunDao.delete(id);
            
            JOptionPane.showMessageDialog(null, "Hapus Data Sukses");
            
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih Data yang akan dihapus");
        }
    }
    
    public void isiTableCariNik() {
        lb = akunDao.getCariNik(frame.getTxtCariNik().getText());
        AkunTableModel tmb = new AkunTableModel(lb);
        frame.getTabelData().setModel(tmb);
    }
    
    public void cariNik() {
        if (!frame.getTxtCariNik().getText().trim().isEmpty()) {
            akunDao.getCariNik(frame.getTxtCariNik().getText());
            isiTableCariNik();
            
        } else {
            JOptionPane.showMessageDialog(frame, "Kolom pencarian tidak boleh kosong");
        }
    }
}
